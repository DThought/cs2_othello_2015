#include "player.h"
#include <climits>
#define CORNER_WEIGHT 64
#define EDGE_WEIGHT 8
#define CORNER_PENALTY 32
#define MAX_ITER 7
#define MIN_ITER 5

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    board = new Board();
    moves = 32;
    self = side;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete board;
}

/*
 * Helper function to find the best possible move on the given side.
 */
Move *getBestMove(Board *board, Side side, int *bestScore, int iter, int alpha) {
    Board test;
    Move *bestMove = new Move(-1, -1);
    Move move(0, 0);
    int score;
    int i, j;
    
    for (i = 0; i < 8; i++) {
        move.setX(i);
        
        for (j = 0; j < 8; j++) {
            move.setY(j);
            
            if (!board->checkMove(&move, side)) {
                continue;
            }
            
            // Copy the board so we can work with it.
            test.copy(board);
            test.doMove(&move, side);
            
            // If we haven't reached the recursion limit, try another layer.
            if (iter) {
                score = -INT_MAX;
                getBestMove(&test, (Side) !side, &score, iter - 1, -*bestScore);
                score = -score;
            } else {
                score = test.count(side) - test.count((Side) !side);
            }
            
            if ((!i || i == 7) && (!j || j == 7)) {
                
                // Weight the score if it's a corner.
                score += CORNER_WEIGHT;
            } else if ((i < 2 || i >= 6) && (j < 2 || j >= 6)) {
                
                // Penalize the score if it's close to a corner.
                score -= CORNER_PENALTY;
            } else if (!i || i == 7 || !j || j == 7) {
                
                // Weight the score if it's on a side.
                score += EDGE_WEIGHT;
            }
            
            // Implement alpha-beta pruning.
            if (score > alpha) {
                delete bestMove;
                *bestScore = score;
                return NULL;
            }
            
            if (score > *bestScore) {
                *bestScore = score;
                bestMove->setX(i);
                bestMove->setY(j);
            }
        }
    }
    
    // If no valid moves exist, pass.
    if (bestMove->getX() == -1) {
        delete bestMove;
        
        if (iter) {
            test.copy(board);
            *bestScore = -INT_MAX;
            getBestMove(&test, (Side) !side, bestScore, iter - 1, INT_MAX);
            *bestScore = -*bestScore;
        } else {
            *bestScore = test.count(side) - test.count((Side) !side);
        }
        
        return NULL;
    }
    
    return bestMove;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    
    // Update the board. Note that Side can be converted into int {0, 1}.
    board->doMove(opponentsMove, (Side) !self);
    
    // Can't use INT_MIN because -INT_MIN == INT_MIN
    int bestScore = -INT_MAX;
    
    // Use MAX_ITER if we have at least five seconds for each remaining move.
    Move *move = getBestMove(board, self, &bestScore,
        msLeft == -1 || msLeft / moves > 5000 ? MAX_ITER : MIN_ITER, INT_MAX);
    
    board->doMove(move, self);
    moves--;
    return move;
}
