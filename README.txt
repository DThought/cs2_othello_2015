This is my AI. I call him John.

John is primarily a minimax algorithm, recursing to either depth 5 or 7 
depending on the amount of time remaining to make the current move, as 
estimated from the total time remaining and the number of moves still to be 
made.

Alpha-beta pruning is used to trim the decision tree for both players' 
anticipated moves. In fact, since one player's score is simply the negative of 
the other player's score, we need only keep track of one value (alpha) for each 
layer. The pruning significantly improves the amount of time spent calculating 
each move, so that the minimax algorithm can recurse to level 7 in an average 
of two seconds.

In addition, simple heuristics are used to calculate the score for each 
potential move. Corner pieces are heavily weighted; in fact, any move that 
takes a corner piece will always be made unless it leads to a corner move by 
the opponent. Moves that may help the opponent take a corner piece (i.e., the 
squares bordering each corner) are also heavily penalized.

As a recursive minimax algorithm, John operates under the assumption that the 
opponent has an identical strategy. This will not always be the case and may 
lead to John making errors when the opponent makes an unexpected move 
(according to different heuristics). Summing the scores given the top two or 
three opponent moves may help alleviate this issue, but was not implemented.
